<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'renegade');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'j>E:~D1oM1Ec;X63cxjB9v-2/&Gv<b44-;$&f-(*g{|V1.Nr5,a?>-YjOC+Rj*f!');
define('SECURE_AUTH_KEY',  '@pX++Gl_zNS)R+%lg{!~xn:FRE~Ex)kE.l>%~s^Yy<sEti0:-BfYHoa@p`fNr_i+');
define('LOGGED_IN_KEY',    '&]Mp6%8[2;= >8R%TkIyImSieo/}:+-m.b,|&$^M&rNw0p:_#Jw%s9%c:;0Y:62C');
define('NONCE_KEY',        'Xv`Q3gEe><?3r;0vV5c+=531#SGdZY(zY5b(ecs+X?VD_SQv$:|@Tw]2#;5X(v(}');
define('AUTH_SALT',        '|:>)rfG0RpZ9zft`{P-xt+*Rd!StU>dBfQmpzHOnwx!FDP$&I^0)2[8(1_)$Q|hq');
define('SECURE_AUTH_SALT', 'Xq9<_,N:H4J+uDsl%Loa++[;cz9D|^C59Lv[7 u&+VX9&!kXChe2Q2A1q?2fO;5:');
define('LOGGED_IN_SALT',   '[6:0>Z{Czl#0ef|5*jM|/ Z2o-SoyXQ>ztRNUal9dEcOlez|lQ+7b+Yu@&T|SF?X');
define('NONCE_SALT',       '*acV+y--+20JMv4D6G;GdAR|G1) AEM[c-!wBA!1;R;X/Oz0A-t^K$%o`{?@t6rT');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Язык локализации WordPress, по умолчанию английский.
 *
 * Измените этот параметр, чтобы настроить локализацию. Соответствующий MO-файл
 * для выбранного языка должен быть установлен в wp-content/languages. Например,
 * чтобы включить поддержку русского языка, скопируйте ru_RU.mo в wp-content/languages
 * и присвойте WPLANG значение 'ru_RU'.
 */
define('WPLANG', 'ru_RU');

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
